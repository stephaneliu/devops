#!/bin/bash
PATH=/usr/bin:/usr/sbin:/bin:/sbin

BACKUP=/mnt/bt-medisoft-data
DESTINATION="spaces:simbak/bt/bt-medisoft"
LOCKFILE=/tmp/rclone_medisoft_lock
LOGFILE=/root/rclone/medisoft/rclone_medisoft.log
EXCLUDES=/root/rclone/medisoft/rclone_medisoft_excludes.txt

if [ ! -e $LOCKFILE ]; then
  touch $LOCKFILE
  rm $LOGFILE

  sshfs root@72.253.148.222:/c/medisoft_data $BACKUP \
  && rclone sync --log-level INFO --log-file=$LOGFILE --exclude-from $EXCLUDES $BACKUP $DESTINATION

  umount $BACKUP

  echo "From: sliu@simpletechawaii.com" | cat - $LOGFILE > /tmp/rclone_medisoft_out && mv /tmp/rclone_medisoft_out $LOGFILE
  mail -s "BT medisoft rclone log" stephane@simplified-technologies.com < $LOGFILE

  rm $LOCKFILE
else
  echo "Lock file detected"
  echo "Previous shared rclone process still running" > $LOCKFILE

  mail -s "BT medisoft rclone log" stephane@simplified-technologies.com < $LOCKFILE
fi
